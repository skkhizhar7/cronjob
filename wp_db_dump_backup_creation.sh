#!/bin/bash
sudo rm -rf /opt/db_backups
sudo mkdir /opt/db_backups/
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
bold=$(tput -Txterm bold);
normal=$(tput -Txterm sgr0);

echo "${bold} =================================================================================== ${normal}"
echo "${bold} ********** STARTED DATABASE BACKUP AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"

TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
echo "${bold} ********** INSTALLING MYSQL CLIENT AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
install_mysql="$(sudo yum install mysql -y)";
if [ $? -ne 0 ]
then
    TODAYINDIA=$(TZ=":Asia/Calcutta" date)
        TODAYUSA=$(TZ=":US/Eastern" date)
        echo "${bold} ########## ERROR OCCURED WHILE INSTALLING MYSQL CLIENT AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        echo $install_mysql;
        #SEND EMAIL
else
    TODAYINDIA=$(TZ=":Asia/Calcutta" date)
        TODAYUSA=$(TZ=":US/Eastern" date)
    echo "${bold} ********** MYSQL CLIENT INSTALLED SUCCESSFULLY AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
        cd /opt/db_backups/
        echo "${bold} ********** STARTED CREATING MYSQLDUMP FOR MAGENTO DB AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"

        now=$(date +"%d_%m_%Y_%H_%M_%S")
        nowzip=$(date +"%d_%m_%Y")

        mysqlfilename="sonybt_wp_prod_$now";
        mysqlzipname="sonybt_wp_prod_$nowzip";
        sudo mysqldump --quick --lock-tables=false -h sonybt-prod.cluster-cnhb0fzurlwq.us-east-1.rds.amazonaws.com -usonybt_prod -pQsRDhhBLqUk6EMr3 sonybt_wp > $mysqlfilename.sql
        if [ $? -ne 0 ]
        then
                TODAYINDIA=$(TZ=":Asia/Calcutta" date)
                TODAYUSA=$(TZ=":US/Eastern" date)
                echo "${bold} ########## ERROR OCCURED WHILE CREATING MYSQLDUMP FOR MAGENTO DB AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
                #SEND EMAIL
        else
                TODAYINDIA=$(TZ=":Asia/Calcutta" date)
                TODAYUSA=$(TZ=":US/Eastern" date)
                echo "${bold} ********** SUCCESSFULLY CREATED MYSQLDUMP FOR MAGENTO DB AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"

                sleep 3
                echo "${bold} ********** STARTED ZIPPING MYSQLDUMP AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
                sudo zip --password 2Xgczjn3QU49Xu6V -q -r $mysqlzipname.zip $mysqlfilename.sql;
                if [ $? -ne 0 ]
                then
                        TODAYINDIA=$(TZ=":Asia/Calcutta" date)
                        TODAYUSA=$(TZ=":US/Eastern" date)
                        echo "${bold} ########## ERROR OCCURED WHILE ZIPPING MYSQLDUMP FOR MAGENTO DB AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
                        #SEND EMAIL
                else
                        TODAYINDIA=$(TZ=":Asia/Calcutta" date)
                        TODAYUSA=$(TZ=":US/Eastern" date)
                        echo "${bold} ********** ZIP CREATED MYSQLDUMP FOR MAGENTO DB AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
                fi

                sleep 3
                echo "${bold} ********** COPYING ZIP TO S3 FOR MAGENTO DB AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
                copied="$(sudo aws s3 cp $mysqlzipname.zip s3://sonybt-prod-backups/db_backups/ )";
                if [ $? -ne 0 ]
                then
                        TODAYINDIA=$(TZ=":Asia/Calcutta" date)
                        TODAYUSA=$(TZ=":US/Eastern" date)
                        echo "${bold} ########## ERROR OCCURED WHILE COPYING ZIP FOR MAGENTO DB TO S3 AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
                        #SEND EMAIL
                else
                        TODAYINDIA=$(TZ=":Asia/Calcutta" date)
                        TODAYUSA=$(TZ=":US/Eastern" date)
                        echo "${bold} ********** COPIED ZIP TO S3 FOR MAGENTO DB AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
                        #SEND EMAIL
                fi


        fi
fi

sleep 3
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
echo "${bold} ********** REMOVING MYSQL CLIENT AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
remove_mysql="$(sudo yum remove mysql -y)";
if [ $? -ne 0 ]
then
    TODAYINDIA=$(TZ=":Asia/Calcutta" date)
        TODAYUSA=$(TZ=":US/Eastern" date)
        echo "${bold} ########## ERROR OCCURED WHILE REMOVING MYSQL CLIENT AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        echo $install_mysql;
        #SEND EMAIL
else
    TODAYINDIA=$(TZ=":Asia/Calcutta" date)
        TODAYUSA=$(TZ=":US/Eastern" date)
    echo "${bold} ********** MYSQL CLIENT REMOVED SUCCESSFULLY AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
fi
#sudo rm -rf /opt/db_backups
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
echo "${bold} ********** COMPLETED DATABASE BACKUP AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
