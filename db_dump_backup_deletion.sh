
TODAYINDIA=$(TZ=":Asia/Calcutta" date)
TODAYUSA=$(TZ=":US/Eastern" date)
bold=$(tput -Txterm bold);
normal=$(tput -Txterm sgr0);
echo "${bold} ================================================================================ ${normal}"
nowzip=$(date --date "15 days ago" +"%d_%m_%Y");
magentomysqlzipname="sonybt_magento_prod_$nowzip";
wpmysqlzipname="sonybt_wp_prod_$nowzip"
sudo aws s3 rm s3://sonybt-prod-backups/db_backups/$magentomysqlzipname.zip
sudo aws s3 rm s3://sonybt-prod-backups/db_backups/$wpmysqlzipname.zip

echo "${bold} **********        STARTED DATABASE DELETION AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
if [ $? -ne 0 ]
then
    TODAYINDIA=$(TZ=":Asia/Calcutta" date)
        TODAYUSA=$(TZ=":US/Eastern" date)
        echo "${bold} ########## ERROR OCCURED WHILE DELETING DB BACKUP FROM S3 AT $TODAYUSA ( $TODAYINDIA ) ########## ${normal}"
        echo $install_mysql;
        #SEND EMAIL
else
    TODAYINDIA=$(TZ=":Asia/Calcutta" date)
        TODAYUSA=$(TZ=":US/Eastern" date)
    echo "${bold} ********** DATABASE BACKUP ($wpmysqlzipname.zip) DELETED SUCCESSFULLY AT $TODAYUSA ( $TODAYINDIA ) ********** ${normal}"
fi
